﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChecklinkPolitica : MonoBehaviour
{

    public Text _text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.frameCount % 60 == 0)
            CheckLanguage();
    }

    private void CheckLanguage() {
        switch (DataBase.db.getLanguage())
        {

            case 0:
                _text.text = "Política de privacitat";
                break;
            case 1:
                _text.text = "Política de privacidad";
                break;
            case 2:
                _text.text = "Privacy policy";
                break;


        }
    }

    public void OpenLink() {

        Application.OpenURL("https://enti.cat/es/politica-de-privacidad/");
    
    }
}
