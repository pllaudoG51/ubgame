﻿using UnityEngine;
using System.Collections;

public class ChangeSpritePassadis : MonoBehaviour {
	public Sprite WithPala;
	public Sprite WithOutPala;

	// Use this for initialization
	void Start () {
		if (DataBase.db.getObject (2))
			ChangePala (true);
		else
			ChangePala (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangePala(bool bolea){
		if (bolea)
			this.GetComponent<SpriteRenderer> ().sprite = WithPala;
		else
			this.GetComponent<SpriteRenderer> ().sprite = WithOutPala;
	}
}
