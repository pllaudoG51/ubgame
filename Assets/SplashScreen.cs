﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {
	public Sprite Image2;
	public Sprite Image3;
	// Use this for initialization
	void Start () {		
		Invoke ("fading1", 2.0f);
		Invoke ("ChangeImage2", 2.35f);
		Invoke ("fading2", 2.5f);
		Invoke ("fading1", 4.75f);
		Invoke ("ChangeImage3", 5.0f);
		Invoke ("fading2", 5.25f);
		Invoke ("fading1", 7.5f);
		Invoke ("ChangeEscene", 7.75f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	private void ChangeImage2(){
		ChangeImage ("Image", Image2);
	}

	private void fading1(){
		fading ("Image", 0.0f, 0.25f);
	}

	private void fading2(){
		fading ("Image", 1.0f, 0.25f);
	}

	private void ChangeImage3(){
		ChangeImage ("Image", Image3);
	}
		

	private void ChangeEscene(){
		SceneManager.LoadScene("Language", LoadSceneMode.Single);
	}

	private void fading(string image, float fade, float time){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Image imageCanvas = canvasObject.transform.Find(image).GetComponent<Image>();
		imageCanvas.GetComponent<Image>().CrossFadeAlpha(fade, time, false);
	}

	private void ChangeImage(string image, Sprite sprite){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Image imageCanvas = canvasObject.transform.Find(image).GetComponent<Image>();
		imageCanvas.GetComponent<Image> ().sprite = sprite;
	}
}
