﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CreditsManager : MonoBehaviour {

    public Camera _cam;
    private Transform myPos;
    public Transform end;
    private bool isPressed;
	// Use this for initialization
	void Start () {
        Time.timeScale = 1;
        if(GameMaster.GM!=null)GameMaster.GM.DestroyGameMaster();
        isPressed = false;
        myPos = _cam.GetComponent<Transform>();
        StartCoroutine(CreditsShown());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

   private  IEnumerator CreditsShown()
    {
        while (myPos.position.y >= end.position.y)
        {
            if (isPressed)
            {
                myPos.position = new Vector3(myPos.position.x, (myPos.position.y - 0.50f), myPos.position.z);
            }
            else
            {
                myPos.position = new Vector3(myPos.position.x, (myPos.position.y - 0.018f), myPos.position.z);
            }
            //per mirar que va be de forma rapida
           // myPos.position = new Vector3(myPos.position.x, (myPos.position.y - 10.0f), myPos.position.z);
            yield return new WaitForSeconds(0.04f);
        }
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("MainMenu");
        yield break;
    }

    public void Pressing( bool _b)
    {
        isPressed = _b;
    }
}
