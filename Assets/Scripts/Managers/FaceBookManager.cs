﻿using UnityEngine;
using Facebook.Unity;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class FaceBookManager : MonoBehaviour {
    public static FaceBookManager FbM;
    public GameObject button;
    public GameObject Endbutton;


    private void Awake() {//singleton
        if (!FB.IsInitialized)
        {
            FB.Init();
        }
        else
        {
            FB.ActivateApp();
        }
        button.SetActive(false);
        Endbutton.SetActive(false);
    }

    private void Start()
    {
        FbM = this;
    }

    public void LogIn()
    {
        FB.LogInWithReadPermissions(callback:OnLogIn);
    }

    private void OnLogIn(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
        }
        else
        {
            Debug.Log("Failed in LogIn");
        }
    }

	public string getString(int _case){
		switch (DataBase.db.getLanguage ()) {
		case 0:
			switch (_case) {
			case 1:
				return "He resolt el puzzle de la sala de Manuscrits a la Universitat de Barcelona";
			case 2:
				return "He resolt el puzzle del Paranimf a la Universitat de Barcelona";
			case 3:
				return "He resolt el puzzle de la Torre de ciències a la Universitat de Barcelona";
			case 4:
				return "He resolt puzzle del Hall a la Universitat de Barcelona";
			case 5:
				return "He superat la prova dels rectors a la Universitat de Barcelona";
			case 6:
				return "He superat la prova de la Universitat de Barcelona";
			}
			break;
		case 1:
			switch (_case) {
			case 1: 
				return "He resuelto el puzzle de la Sala de Manuscritos de la Universidad de Barcelona";
			case 2:
				return "He resuelto el puzzle del Paraninfo de la Universidad de Barcelona";
			case 3:
				return "He resuelto el puzzle de la Torre de ciencias de la Universidad de Barcelona";
			case 4:
				return "He resuelto el puzzle del Hall de la Universidad de Barcelona";
			case 5:
				return "He superado la prueba de los rectores en la Universidad de Barcelona";
			case 6:
				return "He superado la prueba de la Universidad de Barcelona";
			}
			break;
		case 2:
			switch (_case) {
			case 1:
				return "I solved the puzzle of the Manuscript room at the University of Barcelona";
			case 2:
				return "I solved the puzzle of the Assembly Hall at the University of Barcelona";
			case 3:
				return "I solved the puzzle of the Sciencie Tower at the University of Barcelona";
			case 4:
				return "I solved the puzzle of the Hall at the University of Barcelona";
			case 5:
				return "I have passed the test of the rectors at the University of Barcelona";
			case 6:
				return "I have passed the test of the Universitat de Barcelona";
			}
			break;

		}
		return "";
	}

    public void Share( int _case)
    {
		//string text = getString (_case);
        switch (_case)
        {
            case 1://puzzle arxiu
                FB.ShareLink(
                   contentTitle: "Una nit en la Universitat de Barcelona",
					contentURL: new System.Uri("http://www.ub.edu/nitalauni"),
					contentDescription: getString (_case),
					photoURL: new System.Uri("http://www.ub.edu/nitalauni/Fotos/Biblioteca.png"),
                   //photoURL: new System.Uri("https://i.gyazo.com/319efc48be5bc2b563fa0617b8b32202.png"),
           callback: OnShare);
                break;
            case 2://puzzle paraninf
                FB.ShareLink(
                   contentTitle: "Una nit en la Universitat de Barcelona",
					contentURL: new System.Uri("http://www.ub.edu/nitalauni"),
					contentDescription: getString (_case),
					photoURL: new System.Uri("http://www.ub.edu/nitalauni/Fotos/Paranimf.png"),
                   //photoURL: new System.Uri(" https://i.gyazo.com/f69dbfbf08e7a1222008708a1658d0b5.png"),
                   callback: OnShare);
                break;
            case 3://puzzle observatori
                FB.ShareLink(
                   contentTitle: "Una nit en la Universitat de Barcelona",
					contentURL: new System.Uri("http://www.ub.edu/nitalauni"),
					contentDescription: getString (_case),
					photoURL: new System.Uri("http://www.ub.edu/nitalauni/Fotos/Observatori.png"),
                  // photoURL: new System.Uri("https://i.gyazo.com/a66c52ac6622ec2e6e234731adfdad9c.png"),
                   callback: OnShare);
                break;
            case 4://puzzle hall
                FB.ShareLink(
                   contentTitle: "Una nit en la Universitat de Barcelona",
					contentURL: new System.Uri("http://www.ub.edu/nitalauni"),
					contentDescription: getString (_case),
					photoURL: new System.Uri("http://www.ub.edu/nitalauni/Fotos/Hall.png"),
                   //photoURL: new System.Uri("https://i.gyazo.com/747a862ee75c4015386b563d10ca80de.png"),
                   callback: OnShare);
                break;
			case 5://Fantasma
                FB.ShareLink(
					contentTitle: "Una nit en la Universitat de Barcelona",
					contentURL: new System.Uri("http://www.ub.edu/nitalauni"),
					contentDescription: getString (_case),
					photoURL: new System.Uri("http://www.ub.edu/nitalauni/Fotos/Alternatiu1.png"),
					//photoURL: new System.Uri("https://i.gyazo.com/747a862ee75c4015386b563d10ca80de.png"),
					callback: OnShare);
                break;
			case 6://Normal
                FB.ShareLink(
					contentTitle: "Una nit en la Universitat de Barcelona",
					contentURL: new System.Uri("http://www.ub.edu/nitalauni"),
					contentDescription: getString (_case),
					photoURL: new System.Uri("http://www.ub.edu/nitalauni/Fotos/Alternatiu2.png"),
					//photoURL: new System.Uri("https://i.gyazo.com/747a862ee75c4015386b563d10ca80de.png"),
					callback: OnShare);
				break;
            //per cada final potser fer una publi?
            default:
                FB.ShareLink(
                  contentTitle: "ORYPS GAMES",
                  contentURL: new System.Uri("https://www.facebook.com/OrypsGames/"),
                  contentDescription: "Look at that awesome game!!",
                  photoURL: new System.Uri("https://scontent-mad1-1.xx.fbcdn.net/v/t1.0-9/15027770_169094210221560_4656137069304159979_n.png?oh=d920795bb3b66a236b018f83da8afb0f&oe=58BFB3DF"),
                  callback: OnShare);
                break;

        }
        button.SetActive(false);
        Endbutton.SetActive(false);
    }

    public void Ready2Share(int i)
    {
        StartCoroutine(showButton(i));
    }


   private IEnumerator showButton(int _i)
    {
        if (_i == 1)
        {
            button.SetActive(true);
            yield return new WaitForSeconds(5.0f);
            button.SetActive(false);
        }
        else
        {
            Endbutton.SetActive(true);
            yield return new WaitForSeconds(5.0f);
            Endbutton.SetActive(false);
        }
        yield break;
    }

    private void OnShare(IShareResult result)
    {
        if(result.Cancelled || !string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("ShareLink error: " + result.Error);
        }
        else if (!string.IsNullOrEmpty(result.PostId))
        {
            Debug.Log(result.PostId);
        }
        else
        {
            Debug.Log("Share Succeed!");
        }
    }
}
