﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {

	private bool CanWalk=true;
	private Vector3 offset;
	private Click cl;
	private float distance;
	private string ActualDoor;
	private Player pl;
	private bool _changeIDLE = false;
	private int IDLE=0;
	private bool _dialeg = false;//he afegit un boolea per saber quan el dialeg esta actiu, pot ser util, almenys per les pistes segur
	private bool _pause = false;
	//private bool moviment  =false;
	public static GameMaster GM;
	public GameObject click;
	public GameObject Player;

    void Awake() {
        if (GM == null) {
            DontDestroyOnLoad(gameObject);
            GM = this;
			invokePlayer(GameObject.FindGameObjectWithTag("Spawn").transform);
        }
        else if (GM != this) {	
			GameMaster.GM.ChangeScene();
            Destroy(gameObject);
        }
    }
        
    // Use this for initialization
	void Start () {
		
		pl = GameObject.Find ("Player").GetComponent<Player>();
		///hauria d'invocar el player en una posicio per defecte, llavors la creacio del Actual Door es fara correctament
	}

	// Update is called once per frame
	void Update () {

	}

	public void Walk(){
		///destruim qualsevol click anterior
		if(!PuzzleManager.PM.GetActivePuzzle() && CanWalk && !getPause()){
			CheckClick();
			//pl.GetComponent<Animator>().SetBool("Run", true);
			///agafem posicio de la camara respecte escena
			offset =  Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f));
			//creem click
			Instantiate(click, new Vector3 (offset.x, offset.y, 0), Quaternion.identity);
			resetDoors();
		}
	}

	public void Walk(Transform transform, int idle){
		if(!PuzzleManager.PM.GetActivePuzzle() && CanWalk && !getPause()){
			CheckClick();
			//pl.GetComponent<Animator>().SetBool("Run", true);
			///agafem posicio de la camara respecte escena
			Instantiate(click, transform.position, Quaternion.identity);
			resetDoors();
			IDLE = idle;
			_changeIDLE =true;
		}
	}


	private void resetDoors(){
		GameObject[] go = GameObject.FindGameObjectsWithTag ("Door");
		foreach (GameObject door in go){
			door.GetComponent<Door>().resetClicked();
		}
		DataBase.db.RefreshInventory ();//aqui funciona be XD
	}
	private void CheckClick(){
		if (GameObject.Find ("Click(Clone)") != null) {
			cl = GameObject.Find("Click(Clone)").GetComponent<Click>();
			if(cl!=null){
				if(pl!=null)pl.CantMove();
				cl.destroy();
			}
		}
	}

	public void ChangeScene(){
		GameObject[] go = GameObject.FindGameObjectsWithTag ("Door");
		//Debug.Log(ActualDoor);
		foreach (GameObject door in go){
			if(door.GetComponent<Door>().getConnection()==ActualDoor){
				invokePlayer(door.transform.Find("click"));
				string posicio = door.GetComponent<Door>().getPositionIDLE();
				Player pl = GameObject.Find("Player").GetComponent<Player>();
				switch (posicio){
				case "down":	
					pl.DefineIDLE(0);
					break;
				case  "top":
					pl.DefineIDLE(1);
					break;
				case  "right":
					pl.DefineIDLE(2);
					break;
				case  "left":
					pl.DefineIDLE(3);
					break;
				}
				//invocar personatge en el fill
				//agafar la posicio en que esta la porta, i segons aquesta ficar l'animacio

			}
		}

	}

	private void invokePlayer(Transform transform){
		
		GameObject P = Instantiate(Player, transform.position, Quaternion.identity) as GameObject;
		P.name= "Player";
	}

	public void SetCanWalk(bool boolea){
        CanWalk = boolea;
	}

	public bool GetCanWalk(){
		return CanWalk;
	}

	public void setDialeg(bool boolea){
		_dialeg = boolea;
	}

	public bool getDialeg(){
		return _dialeg;
	}

	public void SetActualDoor(string strings){

		ActualDoor=strings;
	}

	public string GetActualDoor(){
		return ActualDoor;
	}

	/////IDLE
	public void changeIDLE(int idle){
		IDLE = idle;
	}

	public void setIDLE(bool boolea){
		_changeIDLE = boolea;
	}

	public int CheckIDLE(){
		_changeIDLE=false;
		return IDLE;
	}

	public bool getChangeIDLE(){
		return _changeIDLE;
	}

	public bool Walking(){
		if(GameObject.Find("Click(Clone)")==null)return false;
		return true;
	}
		
	public void DestroyGameMaster(){
		Destroy(gameObject);
	}

	//pause functions
	public void setPause(bool boolea){
		_pause = boolea;
	}

	public bool getPause(){
		return _pause;
	}
}
