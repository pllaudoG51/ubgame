﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour {
	public GameObject _Pause;
	public GameObject _Language;
	public GameObject _Button;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActivePause(bool boolea){
		_Pause.SetActive (boolea);
        if (boolea) {
            Dialogos.TM.SkipNo();
            ChangeNames ();
			GameMaster.GM.setPause (true);
			ActiveButton (false);
			Time.timeScale = 0;
		} else
			Time.timeScale = 1;
	}

	public void ActiveLanguage(bool boolea){
		_Language.SetActive (boolea);
	}

	public void SetOption(string option){
		switch (option) {
		case "Resume":
			ActivePause (false);
            Dialogos.TM.restartDialeg();
            ActiveButton (true);
			GameMaster.GM.setPause (false);
			CursorManager.CM.OnCursorExit ();
			break;
		case "Main Menu":
			GameMaster.GM.setPause (false);
			ActivePause (false);
			ActiveButton (true);
			DataBase.db.setZone ("Menu");
			Dialogos.TM.StopDialeg ();
			GameMaster.GM.DestroyGameMaster ();
			SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
			break;
		case "Language":
			ActiveLanguage (true);
			_Pause.SetActive (false);
			break;
		case "Quit":
			DataBase.db.setZone ("Menu");
			GameMaster.GM.DestroyGameMaster ();
			switch (DataBase.db.getLanguage()) {
			case 0:
				SceneManager.LoadScene("Credits", LoadSceneMode.Single);
				break;
			case 1:
				SceneManager.LoadScene("ES_Credits", LoadSceneMode.Single);
				break;
			case 2:
				SceneManager.LoadScene("EN_Credits", LoadSceneMode.Single);
				break;
			}
			break;
		}
	}


	public void setLanguage(string option){
		switch (option) {
		case "Catala":
			DataBase.db.setLanguage (0);
			break;
		case "Castella":
			DataBase.db.setLanguage (1);
			break;
		case "Angles":
			DataBase.db.setLanguage (2);
			break;
		}
		ActiveLanguage (false);
		ActivePause (true);
		ChangeNames ();
		CursorManager.CM.OnCursorExit ();
	}


	//funcions per canviar el text dels menus
	private void ChangeNames(){
		switch (DataBase.db.getLanguage ()) {
		case 0:
			changeText ("Pausa", "Pause/Title");
			changeText ("Continuar", "Pause/Botons/Resume/Text");
			changeText ("Menú Principal", "Pause/Botons/Main Menu/Text");
			changeText ("Idioma", "Pause/Botons/Language/Text");
			changeText ("Crèdtis", "Pause/Botons/Quit/Text");
			break;
		case 1:
			changeText ("Pausa", "Pause/Title");
			changeText ("Reanudar", "Pause/Botons/Resume/Text");
			changeText ("Menú Inicial", "Pause/Botons/Main Menu/Text");
			changeText ("Idioma", "Pause/Botons/Language/Text");
			changeText ("Créditos", "Pause/Botons/Quit/Text");
			break;
		case 2:
			changeText ("Paused", "Pause/Title");
			changeText ("Resume", "Pause/Botons/Resume/Text");
			changeText ("Main Menu", "Pause/Botons/Main Menu/Text");
			changeText ("Language", "Pause/Botons/Language/Text");
			changeText ("Credits", "Pause/Botons/Quit/Text");
			break;
		}
	}

	private void changeText(string _text, string path){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Text textCanvas;
		textCanvas = canvasObject.transform.Find(path).GetComponent<Text>();
		textCanvas.text = _text;
	}

	//funcio per activar o desactivar el boto de pausa
	private void ActiveButton(bool boolea){
		_Button.SetActive (boolea);
	}


}
