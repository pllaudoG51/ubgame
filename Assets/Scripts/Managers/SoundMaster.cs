﻿using UnityEngine;
using System.Collections;

public class SoundMaster : MonoBehaviour {

	public AudioSource fxSource;
	public AudioSource musicSource;

	public static SoundMaster instance = null;

	public float lowPitchRange = 0.95f;
	public float highPitchRange = 1.05f;

	public AudioClip GSong;
	public AudioClip PSong;

	public AudioClip Click;
	public AudioClip Character;
	public AudioClip OpenDoor;
	public AudioClip UseDoor;
	public AudioClip GetItem;
	public AudioClip UseItem;
	public AudioClip Victory;
	public AudioClip Error;
    public AudioClip Soroll;

    AudioClip ac;

	// Use this for initialization
	void Awake () {
		if (!instance) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}

	void Update(){
		if(Input.GetMouseButtonDown(0)){//si es vol que el click soni a cad click de ratoli activar aixo
			//PlayClip("Click");
		}
	
	}

	public void PlayClip(string clip){
		
		switch (clip) {
			case "Click":
				ac = Click;
			break;
			case "Character":
				ac = Character;
			break;
			case "OpenDoor":
				ac = OpenDoor;
			break;
			case "UseDoor":
				ac = UseDoor;
			break;
			case "GetItem":
				ac = GetItem;
			break;
			case "UseItem":
				ac = UseItem;
			break;
			case "Victory":
				ac = Victory;
				Change2Puzzle (false);
			break;
			case "Error":
				ac = Error;
			break;
            case "Soroll":
                ac = Soroll;
                break;
		default:
			break;
		}
		fxSource.clip = ac;
		fxSource.Play ();
	}

	public void Change2Puzzle(bool actiu){//cridar en el start del puzzle i en el victory
		if (actiu) {
			musicSource.clip = PSong;
			musicSource.Play ();
		} else if(!actiu){
			musicSource.clip = GSong;
			musicSource.Play ();
		}
	}
}
