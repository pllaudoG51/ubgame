﻿using UnityEngine;
using System.Collections;

public class ClueManager : MonoBehaviour {
	private  int _clue = 1;//la pista per la qual passem
	private int _attempts = 0;
	private int MaxAttempts = 2;
	private bool _active = false;
	private float timerclue = 10.0f;
	private bool time = false;

	public int MaxClue = 4;
	public static ClueManager cM;
	// Use this for initialization
	void Start () {
		cM = this;
	}
	
	// Update is called once per frame
	void Update () {
		if(_active){
			if(timerclue>0)timerclue -= Time.deltaTime;
			if(timerclue<0)timerclue = 0;
			if(timerclue ==0){
				time = true;
				//si volem que s'activi sol
			}
		}
	}

	public void ActiveClue(bool active){
		_active = active;
	}

	public void setAtempt(){
		_attempts++;
		if(_attempts>=MaxAttempts)checkClue();
	}

	private void AddClue(){
		_attempts=0;
		time = false;
		timerclue = 10.0f;
		_clue ++;

	}

	public int getClueNum(){
		setAtempt ();
		return _clue;
	}

	private void checkClue(){
		if (_clue < MaxClue) {
			if (_attempts >= MaxAttempts && time)
				AddClue ();
		} else if (_clue == MaxClue) {
			if (_attempts >= MaxAttempts && time)
			resetClueManager ();
		}
	}

	public void resetClueManager(){
		time = false;
		_clue = 1;
		_active = false;
		timerclue = 30.0f;
		_attempts = 0;
	}
}
