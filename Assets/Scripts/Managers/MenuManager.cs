﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {
	public bool _menuprincipal = true;

	// Use this for initialization
	void Start () {
		if(_menuprincipal)ChangeNames ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetOption(string option){
		switch (option) {
		case "New Game":
                DataBase.db.reset();
                SceneManager.LoadScene("Biblioteca", LoadSceneMode.Single);
			break;
		case "Continue":
                DataBase.db.reset();
                if (DataBase.db.Load()) SceneManager.LoadScene(DataBase.db.getZone(), LoadSceneMode.Single);
			break;
		case "Language":
			SceneManager.LoadScene("Language", LoadSceneMode.Single);
			break;
		case "Credits":
			switch (DataBase.db.getLanguage()) {
			case 0:
				SceneManager.LoadScene("Credits", LoadSceneMode.Single);
				break;
			case 1:
				SceneManager.LoadScene("ES_Credits", LoadSceneMode.Single);
				break;
			case 2:
				SceneManager.LoadScene("EN_Credits", LoadSceneMode.Single);
				break;
			}
            break;
		}
	}

	public void setLanguage(string option){
		switch (option) {
		case "Catala":
			DataBase.db.setLanguage (0);
			break;
		case "Castella":
			DataBase.db.setLanguage (1);
			break;
		case "Angles":
			DataBase.db.setLanguage (2);
			break;
		}
		SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
	}


	//funcions per canviar el text dels menus
	private void ChangeNames(){
		switch (DataBase.db.getLanguage ()) {
		case 0:
			changeText ("Nou Joc", "Botons/New Game/Text");
			changeText ("Continuar", "Botons/Continue/Text");
			changeText ("Idioma", "Botons/Language/Text");
			changeText ("Crèdits", "Botons/Credits/Text");
			break;
		case 1:
			changeText ("Nuevo Juego", "Botons/New Game/Text");
			changeText ("Continuar", "Botons/Continue/Text");
			changeText ("Idioma", "Botons/Language/Text");
			changeText ("Créditos", "Botons/Credits/Text");
			break;
		case 2:
			changeText ("New Game", "Botons/New Game/Text");
			changeText ("Continue", "Botons/Continue/Text");
			changeText ("Language", "Botons/Language/Text");
			changeText ("Credits", "Botons/Credits/Text");
			break;
		}
	}

	private void changeText(string _text, string path){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Text textCanvas;
		textCanvas = canvasObject.transform.Find(path).GetComponent<Text>();
		textCanvas.text = _text;
	}

	public void QuitAplication(){
		Application.Quit ();
	}
}
