﻿using UnityEngine;
using System.Collections;

public class CursorManager : MonoBehaviour {
	public static CursorManager CM;

	private CursorMode cursorMode = CursorMode.ForceSoftware;

	// Use this for initialization
	void Start () {
		CM = this;
		OnCursorExit();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	/// <summary>
	/// Funcio que fa que quan passi per sobre el cursor aquest canvii
	/// </summary>
	/// <param name="type">Type.</param>
	public void OnCursorEnter(string type) {
		if(!GameMaster.GM.getDialeg() && !PuzzleManager.PM.GetActivePuzzle() && !GameMaster.GM.getPause()){
			Texture2D imatge;
			imatge = Resources.Load("HUD/Icons/Cursor/"+type, typeof(Texture2D)) as Texture2D;
			Cursor.SetCursor(imatge, Vector2.zero, cursorMode);
		}
	}

	public void OnCursorEnterPuzzle(string type){
		Texture2D imatge;
		imatge = Resources.Load("HUD/Icons/Cursor/"+type, typeof(Texture2D)) as Texture2D;
		Cursor.SetCursor(imatge, Vector2.zero, cursorMode);
	}
	public void OnCursorExit() {
		//Cursor.SetCursor(null, Vector2.zero, cursorMode);
		Texture2D imatge;
		imatge = Resources.Load("HUD/Icons/Cursor/defect", typeof(Texture2D)) as Texture2D;
		Cursor.SetCursor(imatge, Vector2.zero, cursorMode);
	}
}
