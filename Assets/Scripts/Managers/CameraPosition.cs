﻿using UnityEngine;
using System.Collections;

public class CameraPosition : MonoBehaviour {
	private GameObject pl;
	private bool stop = false;
	private bool once = true;

	public bool passadis = true;
	public static CameraPosition CM;
	public bool vertical = false;
	public GameObject Part1;
	public GameObject Part2;
	// Use this for initialization
	void Start () {
		pl =GameObject.Find("Player");
		CM = this;
	
	}
	
	// Update is called once per frame
	void Update () {
		if(once)CheckSpawn();
		if(!stop){
			if(vertical)transform.position= new Vector3(transform.position.x, pl.transform.position.y, transform.position.z);
			else if(!vertical)transform.position= new Vector3(pl.transform.position.x, transform.position.y, transform.position.z);
		}
	}

	private void CheckSpawn(){
		once = false;
		if(stop)CheckPosition();

	}

	private void CheckPosition(){
		if(passadis){
			if (Vector2.Distance(Part1.transform.position,pl.transform.position) < Vector2.Distance(Part2.transform.position, pl.transform.position))transform.position = Part1.transform.position;
			else transform.position = Part2.transform.position;
		}
	}

	public void setStop(bool boolea){
		stop = boolea;
	}
}
