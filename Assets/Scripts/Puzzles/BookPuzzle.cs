﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BookPuzzle : MonoBehaviour {
	public string FinalText;//ho fico en public no se si s'hauria de llegir del JSON
	public int TotalWords;//son les paraules que necessitem per completar el puzzle
	public string[] Words;//totes les paraules que tenim disponibles
	public GameObject _invoke;


	private string ActualText;
	private string _paintText;
	private int NumberWords = 0;//paraules que portem seleccionades
	private bool[] WordsUsed;//vol dir si ja em fet servir les paraules

	// Use this for initialization
	void Start () {
		WordsUsed = new bool[Words.Length];
		initWordsUsed();
	}
	
	// Update is called once per frame
	void Update () {
	}


	/// <summary>
	/// Funcio que inicialitza les paraules usades, també s'utilitza com a reset
	/// </summary>
	private void initWordsUsed(){
		for (int i = 0; i < WordsUsed.Length;i++){
			WordsUsed[i] = false;
		}
	}

	/// <summary>
	/// Definim la paraula que utilitzem però abans filtrem que no estigui en us la paraula
	/// </summary>
	/// <param name="word">Word.</param>
	public void SetWord(string word){
		if(!GameMaster.GM.getDialeg()){
			SoundMaster.instance.PlayClip("Click");
			if(CheckWord(word)){
				_paintText += word + " ";
				ActualText += word;
				NumberWords++;
				paintText("BookPuzzle/TextFinal/Text1", _paintText);
				ChangeImage("Sprites/HUD/BookPuzzle/Subratllat","BookPuzzle/Background/"+word);
				CheckEndPuzzle();
			}
		}
	}


	/// <summary>
	/// Funcio que comprova si una paraula s'ha utilitzat sino pues no retorna false, sino retorna a true i fem el check de la paraula al array
	/// </summary>
	/// <returns><c>true</c>, if word was checked, <c>false</c> otherwise.</returns>
	/// <param name="word">Word.</param>
	private bool CheckWord(string word){
		for (int i = 0; i < Words.Length; i++){
			if(word == Words[i]){
				if(!WordsUsed[i]){
					WordsUsed[i]=true;
					return true;
				}
			}
		}
		return false;
	}

	/// <summary>
	/// Mirem si el puzzle s'ha acabat, primer si s'ha triat les 3 paraules i segon si el text final es el correcte
	/// </summary>
	private void CheckEndPuzzle(){
		if(TotalWords == NumberWords){
			if (ActualText == FinalText) {
				EndPuzzle ();
			}
			else {
				StartCoroutine (CooldownError ());
				GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("Error");
			}
		}
	}

	/// <summary>
	/// Fiquem les variables a 0 per a que el puzzle 
	/// </summary>
	private void resetPuzzle(){
		//no se si s'hauria de cridar un dialeg del historiador i que digues, tu imbecil, això esta malament
		//o sino algun feedback del plan eres tontaco
		initWordsUsed();
		resetHUD();
		ActualText = "";
		_paintText = "";
		NumberWords = 0;
	}

	private void resetHUD(){
		paintText("BookPuzzle/TextFinal/Text1" ,"");
		for (int x = 0; x < Words.Length;x++){
			ChangeImage("Sprites/HUD/BookPuzzle/fons","BookPuzzle/Background/"+Words[x]);
		}
	}

	/// <summary>
	/// funcio que finalitza el puzzle
	/// </summary>
	private void EndPuzzle(){
		SoundMaster.instance.PlayClip("Victory");
		//s'haura de cridar un dialeg o alguna cosa per a que s'acabi tot
		DataBase.db.EndPuzzle(0);
        DataBase.db.setSeqPuzzle(true);
		PuzzleManager.PM.SetActivePuzzle(false);
		PuzzleManager.PM.setComplete();
		Dialogos.TM.activarsequencia("AfterPuzzle");
		CursorManager.CM.OnCursorExit();
		_invoke.SetActive(true);
        FaceBookManager.FbM.Ready2Share(1);
    }

	public void closePuzzle(){
		if (!GameMaster.GM.getDialeg ()) {
			CursorManager.CM.OnCursorExit ();
			GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().Change2Puzzle (false);
			resetPuzzle ();
			PuzzleManager.PM.SetActivePuzzle (false);
			ClueManager.cM.ActiveClue (false);
		}
		//ClueManager.cM.resetClueManager(); no se si ficar-ho
	}
	/// <summary>
	/// Funcions per defecte per canviar text i canviar les imatges
	/// </summary>

	private void paintText(string pathcanvas, string text){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Text name = canvasObject.transform.Find(pathcanvas).GetComponent<Text>();
		name.text =  text;
	}

	private void ChangeImage(string pathimage, string pathcanvas){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Image imageCanvas = canvasObject.transform.Find(pathcanvas).GetComponent<Image>();
		Sprite imatge;
		imatge = Resources.Load(pathimage, typeof(Sprite)) as Sprite;
		imageCanvas.sprite = imatge;
	}

	private IEnumerator CooldownError(){
		ClueManager.cM.setAtempt();
		yield return new WaitForSeconds (1.0f);
		//aixo es per mi per a veure el resultat i que després es reinici, substituir per un dialeg o  el que fiquem (parlar-ho)
		resetPuzzle();
		yield break;
	}
}
