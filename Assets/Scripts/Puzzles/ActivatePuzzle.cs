﻿using UnityEngine;
using System.Collections;

public class ActivatePuzzle : MonoBehaviour {
	public int _id;
	public Transform _click;
	public int idle;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
	}

	void OnMouseDown(){
		if(!DataBase.db.CheckPuzzle(_id)){
			if(!PuzzleManager.PM.GetActivePuzzle()){
				if (Vector2.Distance(transform.position, GameObject.Find("Player").transform.position)<1.5f){
					if(_id == 2){
                        if (DataBase.db.getDug()) Activation();
                        else {
                            Dialogos.TM.activarCartell("Cavar");
                        }
					}
					else Activation();
				}
				else 	GameMaster.GM.Walk(_click, idle);
			}
		}

	}

	void OnMouseEnter(){
		if(!PuzzleManager.PM.GetActivePuzzle() && !DataBase.db.CheckPuzzle(_id))CursorManager.CM.OnCursorEnter("interactive");
	}

	void OnMouseExit(){
		if(!PuzzleManager.PM.GetActivePuzzle() && !DataBase.db.CheckPuzzle(_id))CursorManager.CM.OnCursorExit();
	}

	public void Activation(){
		GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().Change2Puzzle (true);
        Dialogos.TM.activarsequencia("PuzzleExplain");
		PuzzleManager.PM.SetActivePuzzle(true);
		ClueManager.cM.ActiveClue(true);
		CursorManager.CM.OnCursorExit();
		if(_id ==1)GameObject.Find("PicturePuzzle").GetComponent<PicturePuzzle>().ActivateOption();
		if(_id==2)GameObject.Find("PuzzleObservatori").GetComponent<puzzleQuimic>().ActivateOption();
	}
}
