﻿using UnityEngine;
using System.Collections;

public class PuzzleManager : MonoBehaviour {
	//Ens dira quan activem i desactivem puzzles
	public GameObject Puzzle;
	public static PuzzleManager PM;
	public GameObject Characters;
	public bool _puzzle = true;
	public int personatge = 0;
	public GameObject _object;//objecte del puzzle
	public int _id;
	public int _idObject;

	private bool complete = false;
	private bool ActivePuzzle = false;
	// Use this for initialization
	void Start () {
		PM = this;
		if(_puzzle)CheckObject ();
	}

	// Update is called once per frame
	void Update () {

	}

	private void CheckObject(){
		if(DataBase.db.CheckPuzzle(_id)){
			if (DataBase.db.CheckObject (_idObject))_object.SetActive (true);
		}
	}

	public void ActivatePuzzle(string puzzle){
		//activara el puzzle en questió en el canvas
	}

	//getters i setters del boolea de puzzle actiu
	public void SetActivePuzzle(bool active){
		if(!complete){
			ActivePuzzle=active;
			Puzzle.SetActive(active);
			Characters.SetActive(active);
		}
	}

	public bool GetActivePuzzle(){
		return ActivePuzzle;
	}

	public void setComplete(){
		complete = true;
	}

	public int getPersonatge(){
		return personatge;
	}
}
