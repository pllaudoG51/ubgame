﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PicturePuzzle : MonoBehaviour {
	private int[] _Result = new int[4]; 
	private int[] _Options = new int[4]; //lo que anira triant el jugador
	private bool _activate=false;

	public GameObject _invoke;
	// Use this for initialization
	void Start () {
		initOptions();
		setResult();
		ResetSlots(0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void  setResult(){
		_Result[0] = 1;
		_Result[1] = 3;
		_Result[2] = 8;
		_Result[3] = 6;
	}


	public void setOption(int id){
		if(!GameMaster.GM.getDialeg()){
			if( _activate){
				SoundMaster.instance.PlayClip("Click");
				if(!checkIsUsed(id)){
					for (int i = 0; i < _Options.Length; i++){
						if(_Options[i] == 0){//vol dir que hi ha un lloc buit
							_Options[i] = id;
							ChangeOption(id, i);
							CheckFull();
							//paint();
							return;
						}
					}
				}
			}
		}
	}

	private void ChangeOption(int id, int id2){
		fading("PuzzlePicture/SlotsOptions/Quadre"+id, 0.0f, 2.0f);
		ChangeImage("PuzzlePicture/Slots/Slot"+(id2+1), "Puzzles/Peces/peça"+id);
		fading("PuzzlePicture/Slots/Slot"+(id2+1), 1.0f, 2.0f);

	}
	/// <summary>
	/// Funcio que comprova si esta utilitzat
	/// </summary>
	/// <returns><c>true</c>, if is used was checked, <c>false</c> otherwise.</returns>
	/// <param name="id">Identifier.</param>
	private bool checkIsUsed(int id){
		for (int i = 0; i < _Options.Length; i++){
			if(id == _Options[i])return true;
		}
		return false;
	}
	/// <summary>
	/// Funcio que checkeja
	/// </summary>
	private void CheckFull(){//checkeja si tot esta ple,i si esta ple, si es el correcte
		for (int  i = 0; i < _Options.Length;i++){
			if(_Options[i] ==0)return;//si tenim algun espai sense omplir voldra dir que encara no s'ha omplert tot
		}
		if (CheckEnd ())
			EndPuzzle ();
		else {
			ResetPuzzle ();
			GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("Error");
		}
	}

	private void EndPuzzle(){
		StartCoroutine(CooldownVictory());
	}

	private IEnumerator CooldownVictory(){
		GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("Victory");
		yield return new WaitForSeconds(2.0f);
		DataBase.db.EndPuzzle(1);
		CursorManager.CM.OnCursorExit();
		PuzzleManager.PM.SetActivePuzzle(false);
		PuzzleManager.PM.setComplete();
		_invoke.SetActive(true);
        FaceBookManager.FbM.Ready2Share(1);
        Dialogos.TM.activarsequencia("AfterPuzzle");
        yield break;
        //s'haura d'afegir un objecte
        //dialeg i etc
       
    }

	private void ResetPuzzle(){
		StartCoroutine(CooldownReset());
	}

	private IEnumerator CooldownReset(){
		yield return new WaitForSeconds(2.0f);
		initOptions();
		resetFading();
		ResetSlots(1.0f);
		yield break;
	}

	private bool CheckEnd(){
		for (int i = 0; i < _Options.Length; i++){
			if(_Options[i] !=_Result[i])return false;
		}
		return true;
	}


	private void paint(){
		for (int  i = 0; i < _Options.Length;i++){
			Debug.Log(_Options[i]);
		}
	}


	private void initOptions(){
		for (int  i = 0; i < _Options.Length;i++){
			_Options[i] =0;
		}
	}
	//funcions pel cursors


	public void OnEnter(int id){
		if(!checkIsUsed(id))CursorManager.CM.OnCursorEnterPuzzle("puzzle");
	}

	public void OnExit(){
		CursorManager.CM.OnCursorExit();
	}


	/////////Funcions Pintat////////////////////

	private void ChangeImage(string path, string image){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Image imageCanvas = canvasObject.transform.Find(path).GetComponent<Image>();
		Sprite imatge;
		imatge = Resources.Load(image, typeof(Sprite)) as Sprite;
		imageCanvas.sprite = imatge;

	}

	public void closePuzzle(){
		if (!GameMaster.GM.getDialeg ()) {
			OnExit ();
			GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().Change2Puzzle (false);
			initOptions ();
			resetFading ();
			closeSlots ();
			ResetSlots (0.0f);
			PuzzleManager.PM.SetActivePuzzle (false);
			ClueManager.cM.ActiveClue (false);
			_activate = false;
		}
		//ClueManager.cM.resetClueManager(); no se si ficar-ho
	}

	private void resetFading(){
		for (int i= 1; i <= 8; i++){
			fading("PuzzlePicture/SlotsOptions/Quadre"+i, 1.0f, 2.0f);
		}
	}

	private void ResetSlots(float time){
		for (int i= 1; i <= 4; i++){
			fading("PuzzlePicture/Slots/Slot"+i, 0.0f, time);
		}
	}

	private void closeSlots(){
		for (int i= 1; i <= 4; i++){
			ChangeImage("PuzzlePicture/Slots/Slot"+i, "Puzzles/Observatori/fons");
		}
	}


	/// <summary>
	/// funcions per a fer un efecte fading
	/// </summary>
	//0 en fade es per apagar-lo i 1 per encendre'l
	private void fading(string image, float fade, float time){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Image imageCanvas = canvasObject.transform.Find(image).GetComponent<Image>();
		imageCanvas.GetComponent<Image>().CrossFadeAlpha(fade, time, false);
	}

	public void ActivateOption(){
		StartCoroutine (CooldownActivate());
	}

	private IEnumerator CooldownActivate(){
		yield return new WaitForSeconds(0.25f);
		_activate = true;
		yield break;
	}

	//funcions pere remoure opció

	public void RemoveOption(int pos){
		if(_Options[pos] != 0){
			int option = _Options[pos];
			_Options[pos] = 0;
			fading("PuzzlePicture/Slots/Slot"+(pos+1), 0.0f, 2.0f);
			fading("PuzzlePicture/SlotsOptions/Quadre"+option, 1.0f, 2.0f);
		}
	}
}
