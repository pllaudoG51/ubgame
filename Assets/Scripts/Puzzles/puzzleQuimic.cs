﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class puzzleQuimic : MonoBehaviour {
	//definir el tamany de cada cosa
	//private int[] _Options =  new int[36]; de moment no ho fare per evitar fer un array i agafar memoria
	public GameObject _taula;
	public GameObject _close;

	public int[] _FinalResult =  new int[14];
	public  int[] _ActualResult = new int[14];
	private  int ActualPosition = 0;
	private int changePosition = 0;
	private bool change = false;
//	private int value = 0;//per saber el valor a l'hora de clicar
	private bool cantchangeSprite = false;
	private bool _end = false;
	private bool _ActiveTable;
	private bool _activate = false;

	// Use this for initialization
	void Start () {
		StartResult();
	}
	
	// Update is called once per frame
	void Update () {

	}
	//no mola aquesta manera de fer-ho però a no se que ens animem a fer-ho per json no hay mas tu tia
	private void StartResult(){
		_FinalResult[0] = 20;
		_FinalResult[1] = 21;
		_FinalResult[2] = 10;
		_FinalResult[3] = 1;
		_FinalResult[4] = 15;
		_FinalResult[5] = 47;
		_FinalResult[6] = 23;
		_FinalResult[7] = 50;
		_FinalResult[8] = 8;
		_FinalResult[9] = 9;
		_FinalResult[10] = 5;
		_FinalResult[11] = 6;
		_FinalResult[12] = 7;
		_FinalResult[13] = 19;

	}


	/// <summary>
	/// funcio que determina l'accio a fer quan es selecciona una opcio
	/// </summary>

	public void setOption(int option){
		//basicament l'operacio es la seguent
		//primer mirem si el boolea de canvi esta actiu, cosa que vol dir que abans s'ha desclicat una casella
		//si ho esta canviem el resultat i ja esta
		//sino mirem si esta actiu l'opcio 
		//Debug.Log(option);
		if(!GameMaster.GM.getDialeg()){
			if(_activate){
				if(!_ActiveTable){
					//value = option;
					cantchangeSprite = false;
					if(change){
						if(!checkUsed(option))changeResult(option);
						else cantchangeSprite = true;
						return;
					}
					else{

						if(checkUsed(option))removePosition(option);
						else {
							setResult(option);
						}
						return;
					}
				}
			}
		}
	}
	/// <summary>
	/// comprovem si l'opcio esta feta servir o no
	/// </summary>

	private bool checkUsed(int option){
		for (int i = 0; i < _ActualResult.Length;i++){
			if(option == _ActualResult[i])return true;
		}
		return false;
	}

	/// <summary>
	/// funcio que fa que es canvii el seleccionat en posicio actual, donat la posicio seleccionada al remoure
	/// </summary>
	/// <param name="option">Option.</param>
	private void changeResult(int option){
	//	Debug.Log ("aixo es changeresult" + changePosition);
		_ActualResult[changePosition] = option;
		if(ActualPosition==_FinalResult.Length)CheckEnd();
		change = false;
	}
	/// <summary>
	/// fixem el resultat
	/// </summary>
	/// <param name="option">Option.</param>
	private void setResult(int option){
		_ActualResult[ActualPosition] = option;
		if(ActualPosition==_FinalResult.Length-1)CheckEnd();
		else ActualPosition++;
	}
	//changeposition es el valor de la posicio de l'array que canviarem a continuacio
	private void removePosition(int position){
		//Debug.Log ("Aixo es remove position" + ActualPosition);
		for (int i = 0; i < _ActualResult.Length;i++){
			if(position==_ActualResult[i]){
				changePosition = i;
				change = true;
				return;
			}
		}

		//canviem l'sprite de la casella
	}


	public void setPosition(int position){
		//canviarem la imatge del lloc en questio, haurem de veure si s'ha de desactivar o no
		//a partir d'un int que ens dira el valor
		if (!GameMaster.GM.getDialeg ()) {
			if (_activate) {
				if (!_ActiveTable) {
					if (!_end) {
						SoundMaster.instance.PlayClip ("Click");
						if (!cantchangeSprite) {
							if (change) {
								ChangeImage ("Puzzles/Observatori/fons", "PuzzleQuimic/Opcio" + position);
							} else
								ChangeImage ("Puzzles/Observatori/actiu", "PuzzleQuimic/Opcio" + position);
						} else
							cantchangeSprite = false;
					}
					_end = false;
				}
			}
		}
	}

	private void ChangeImage(string pathimage, string pathcanvas){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Image imageCanvas = canvasObject.transform.Find(pathcanvas).GetComponent<Image>();
		Sprite imatge;
		imatge = Resources.Load(pathimage, typeof(Sprite)) as Sprite;
		imageCanvas.sprite = imatge;
	}

	private void CheckEnd(){
		for (int i = 0; i < _FinalResult.Length;i++){
			if(_FinalResult[i]!=_ActualResult[i]){
				GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("Error");
				StartCoroutine(CooldownReset());
				return;
			}
		}
		EndPuzzle();
	}

	private IEnumerator CooldownReset(){
		//yield return new WaitForSeconds(0.1f);
		_end = true;
		yield return new WaitForSeconds(1.0f);
		resetPuzzle();
		yield break;
	}

	private void resetPuzzle(){
		ActualPosition = 0;
		resetActualResult();
		resetSprites();
	}

	private void resetSprites(){
		for (int i = 0; i < 36; i++)ChangeImage("Puzzles/Observatori/fons","PuzzleQuimic/Opcio"+(i+1));
	
	}

	private void resetActualResult(){
		for (int i = 0; i < _ActualResult.Length;i++){
			_ActualResult[i] = 0;
		}
	}

	public void ActiveTaula(bool boolea){
		_close.SetActive(!boolea);
		_taula.SetActive(boolea);
		_ActiveTable = boolea;
	}


	public void closePuzzle(){
		if (!GameMaster.GM.getDialeg ()) {
			CursorManager.CM.OnCursorExit ();
			GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().Change2Puzzle (false);
			resetPuzzle ();
			PuzzleManager.PM.SetActivePuzzle (false);
			ClueManager.cM.ActiveClue (false);
			_activate = false;
		}
	}

	private void EndPuzzle(){
		//afegir element que hi haura al prefab (fer el prefab)
		DataBase.db.checkEspecialAdd(2);
		GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("Victory");
		_activate = false;
		DataBase.db.EndPuzzle(2);
		CursorManager.CM.OnCursorExit();
		PuzzleManager.PM.SetActivePuzzle(false);
		PuzzleManager.PM.setComplete();
        Dialogos.TM.activarsequencia("AfterPuzzle");
        FaceBookManager.FbM.Ready2Share(1);
        //s'hauria de fer una funcio per afegir el objecteen questio
    }

	public void ActivateOption(){
		StartCoroutine(CooldownActivate());
	}

	private IEnumerator CooldownActivate(){
		yield return new WaitForSeconds(0.25f);
		_activate = true;
		yield break;
	}
}
