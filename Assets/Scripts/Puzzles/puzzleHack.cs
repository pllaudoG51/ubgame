﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class puzzleHack : MonoBehaviour {
	private const int MAX_CHARS = 9;//la llargada maxima que tindra que completar el jugador
	private char[] _ABC = new char[26];//array de possibles respostes desde el teclat
	private string _keyWord = "BARCELONA";//resposta correcta
	public string _myString;//resposta final en forma de string per comparar
	public Text _console;
	// Use this for initialization
	void Start () {
		ABCinitalize ();
		PrintF ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void ABCinitalize(){
		_ABC[0] = 'A';
		_ABC[1] = 'B';
		_ABC[2] = 'C';
		_ABC[3] = 'D';
		_ABC[4] = 'E';
		_ABC[5] = 'F';
		_ABC[6] = 'G';
		_ABC[7] = 'H';
		_ABC[8] = 'I';
		_ABC[9] = 'J';
		_ABC[10] = 'K';
		_ABC[11] = 'L';
		_ABC[12] = 'M';
		_ABC[13] = 'N';
		_ABC[14] = 'O';
		_ABC[15] = 'P';
		_ABC[16] = 'Q';
		_ABC[17] = 'R';
		_ABC[18] = 'S';
		_ABC[19] = 'T';
		_ABC[20] = 'U';
		_ABC[21] = 'V';
		_ABC[22] = 'W';
		_ABC[23] = 'X';
		_ABC[24] = 'Y';
		_ABC[25] = 'Z';
		//26 remove
		//27 enter
	}

	private  void checkAnswer (){//click on Enter amd check if it's correct
		if (_myString == _keyWord) {//mirem si coincideix
			EndPuzzle();
		} else {
			GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("Error");
			ResetPuzzle();
		}
	}

	private  void AddLetter(int i){//click on any letter to add it to the console
		if (_myString.Length < MAX_CHARS) {
			_myString += _ABC [i];
		}
	}

	private void RemoveLetter(){//click on Delete to remove the last letter of the console
		//mirar si tenim alguna lletra davant per borrar
		if (_myString.Length >= 1) {
			_myString = _myString.Remove (_myString.Length - 1);
		}
	}

	private void voidArray(){//Remove all the imputs
		_myString += " ";
		_myString = _myString.Remove (0);
	}

	private void PrintF(){//draw your string by console
		if (_myString.Length+1 <= MAX_CHARS) {
			_console.text = _myString + "_";
		} else {
			_console.text = _myString;
		}
	}

	public void ClickKey(int i){
		
		SoundMaster.instance.PlayClip("Click");
		if(!GameMaster.GM.getDialeg()){
			if (i < 26) {//lletra
				AddLetter (i);
			} else if (i == 26) {//delete
				RemoveLetter ();
				//PrintF ();
			} else if (i == 27) {//enter
				checkAnswer ();
			}
			PrintF ();
		}
	}

	public void ClosePuzzle(){//click on top screen cross to close the tab on canvas
		if (!GameMaster.GM.getDialeg ()) {
			ResetPuzzle ();
			GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().Change2Puzzle (false);
			PuzzleManager.PM.SetActivePuzzle (false);
			ClueManager.cM.ActiveClue (false);
		}
	}

	private void ResetPuzzle(){//reset all what is done in the puzzle
		voidArray();
		PrintF ();
	}

	private void EndPuzzle(){//solve the puzzle amd achive your goals
		GameObject.Find("End").GetComponent<Door>().OpenDoor();
		GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("Victory");
		DataBase.db.EndPuzzle(3);
		CursorManager.CM.OnCursorExit();
		PuzzleManager.PM.SetActivePuzzle(false);
		PuzzleManager.PM.setComplete();
        //lo que tenga que passar aqui
        Dialogos.TM.activarsequencia("AfterPuzzle");
        DataBase.db.setEnd(0,true);
        FaceBookManager.FbM.Ready2Share(1);
    }

}
