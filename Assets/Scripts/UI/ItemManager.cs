﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

public class ItemManager : MonoBehaviour {
	private int Sloter;
	public GameObject Selector;
	public Item aux;
	private bool AnyItemSelected = false;

	public static ItemManager IM;
	public Canvas myCanvas;

	// Use this for initialization
	void Start () {
		IM = this;
		DataBase.db.RefreshInventory ();
	
	}

	// Update is called once per frame
	void Update () {
	
	}
	public void SpawnSelector(int slot){
		if(!GameMaster.GM.getDialeg() && !GameMaster.GM.getPause()){
			StopAllCoroutines();
			Sloter = slot;
			if (slot <= DataBase.db.ItemCount()) {
				//Debug.Log(GameObject.Find ("DataBase").GetComponent<DataBase> ().ItemCount());
				Selector.SetActive (true);
				GameObject selector = GameObject.Find("Inventory/Slot"+slot);
				Selector.transform.localPosition = new Vector3(selector.transform.localPosition.x, Selector.transform.localPosition.y, Selector.transform.localPosition.z);
				//quin sprite fiquem
				aux  = DataBase.db.UseItem(Sloter-1);
				if(aux.objective == "") ChangeSpriteSelector("HUD/Selector/complet");
				else {
					if(GameObject.Find(aux.objective) == null)ChangeSpriteSelector("HUD/Selector/gris");
					else{
						if (Vector2.Distance(GameObject.Find(aux.objective).transform.position, GameObject.Find("Player").transform.position) < aux.distance) ChangeSpriteSelector("HUD/Selector/complet");
						else ChangeSpriteSelector("HUD/Selector/gris");
					}
				}
				StartCoroutine(CooldownSelector());
			}
		}
	}
	private IEnumerator CooldownSelector(){
		GameMaster.GM.SetCanWalk (false);
		yield return new WaitForSeconds(1.25f);
		if (!Dialogos.TM.dialegactiu)GameMaster.GM.SetCanWalk (true);
		Selector.SetActive(false);
		yield break;
	}

	private void ChangeSpriteSelector(string path){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Image imageCanvas = canvasObject.transform.Find("Selector").GetComponent<Image>();
		Sprite imatge;
		imatge = Resources.Load(path, typeof(Sprite)) as Sprite;
		imageCanvas.sprite = imatge;
	}


    public void UseItem() {//from 1 to 7
    	if(!PuzzleManager.PM.GetActivePuzzle()){
	        Selector.SetActive(false);
			aux = DataBase.db.UseItem(Sloter - 1);
	        if (aux.objective == "") {
	            //voldra dir que no has d'estar a certa distancia del objectiu-> no se si sera necessari
				//nomes per la part de la clau
				Dialogos.TM.activarCartell("PartClauUse");
	        }
	        else {
				if (GameObject.Find(aux.objective) == null) NotCorrectlyUsed();
	            else {
	
	                if (Vector2.Distance(GameObject.Find(aux.objective).transform.position, GameObject.Find("Player").transform.position) < aux.distance) Invoke(aux._Name, 0);
	                else NotCorrectlyUsed();
	            }
	        }
	        CursorManager.CM.OnCursorExit();//per cancelar el tipus de curosr
		}
    }

    private void NotCorrectlyUsed() {
        switch (aux.objective) {
            case "Biblioteca-Arxiu":
			if(GameObject.Find("Biblioteca-Hall") != null){
	                if (Vector2.Distance(GameObject.Find("Biblioteca-Hall").transform.position, GameObject.Find("Player").transform.position) < 1.5f) Dialogos.TM.activarsequencia("KeyToDoorWalkway");
	                else Dialogos.TM.activarCartell("Nope");
			}
			else Dialogos.TM.activarCartell("Nope");
                break;
			case "CaixaObservatori":
                Dialogos.TM.activarCartell("BadPala");
			break;
			case "Passadis-Hall":
				Dialogos.TM.activarCartell("ClauFinalUse");
			break;
            default:
                Dialogos.TM.activarCartell("Nope");
                break;
        }

    }

    public void ExaminateItem(){
		if(!PuzzleManager.PM.GetActivePuzzle()){
			Selector.SetActive (false);
			//Debug.Log ("Examinated");
			DataBase.db.ExaminateItem(Sloter-1);
			CursorManager.CM.OnCursorExit ();
		}
	}

	///getters i setters
	/// 
	public void setAnyItemSelected(bool boolea){
		AnyItemSelected = boolea;
	}

	public bool getAnyItemSelected(){
		return AnyItemSelected;
	}

	/////Funcions mestres objectes ///////
	/// 
	private void ClauArxiu(){
		SoundMaster.instance.PlayClip("UseItem");
		GameObject.Find(aux.objective).GetComponent<Door>().OpenDoor();
		Remove();
	}

	private void LlibreClau(){
		SoundMaster.instance.PlayClip("UseItem");
        GameObject.Find(aux.objective).GetComponent<Door>().OpenDoor();
        Remove();
        StartCoroutine(CooldownLlibreClau());
	}

	private void Pala(){
		SoundMaster.instance.PlayClip("UseItem");
		DataBase.db.setDug(true);
		Dialogos.TM.activarCartell("PalaCaixa");
		Remove();
	}
	private IEnumerator CooldownLlibreClau(){
		yield return new WaitForSeconds(0.75f);
		//Dialogos.TM.activarsequencia("stuffs");
		yield break;
	}

	private void ClauFinal(){
		SoundMaster.instance.PlayClip("UseItem");
		GameObject.Find(aux.objective).GetComponent<Door>().OpenDoor();
		Remove();
	}

	private void Remove(){
		DataBase.db.RemoveItem (Sloter-1);
	}

	//CheckObjects


	public bool CheckObject(string _name){
		return DataBase.db.CheckObject(ConvertID(_name));
	}

	public void setObject(string _name){
		DataBase.db.SetObjects(ConvertID(_name));
		GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("GetItem");

	}

	private int ConvertID(string _name){
		switch(_name){
		case "ArxiuClau":
			return 0;
		case "LlibreClau":
                Dialogos.TM.activarsequencia("GrabKey");
                return 1;
		case "Pala":
			return 2;
		case "PartClau1":
                Dialogos.TM.examineItem("PartClau");
                return 3;
		case "PartClau2":
                Dialogos.TM.examineItem("PartClau");
                return 4;
		}
		return -1;
	}

}

