﻿using UnityEngine;
using System.Collections;

public class Fading : MonoBehaviour {
	public Texture2D fadeOutTexture;
	public float fadeSpeed = 0.2f;
	public static Fading FD;

	private int drawDepth = -1000;
	public float alpha = 1.0f;
	private int fadeDir = -1; 
	// Use this for initialization

	void Start(){
		if (FD == null){
			DontDestroyOnLoad(gameObject);
			FD = this;
		}
		else if (FD != this){
			Destroy(gameObject);
		}
	}
	void OnGUI(){
		alpha += fadeDir * fadeSpeed * Time.deltaTime;
		alpha = Mathf.Clamp01 (alpha); 
		GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
		GUI.depth = drawDepth;
		GUI.DrawTexture (new Rect(0,0,Screen.width,Screen.height),fadeOutTexture);
	}

	public float BeginFade(int direction){
		fadeDir = direction;
		return (fadeSpeed);
	}

	public void canvidemapacrida(){
		StartCoroutine(LoadScene());

	}

	private  IEnumerator canvidemapa(){
		BeginFade (1);
		yield return new WaitForSeconds(0.5f);
		BeginFade (-1);
		yield break;
	}

	private  IEnumerator LoadScene(){
		BeginFade (1);
		yield return new WaitForSeconds(1.5f);
		BeginFade (-1);
		yield break;
	}

}
