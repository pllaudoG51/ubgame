﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Cutscene : MonoBehaviour {
    public bool _historiador = true;

    private bool _end;
    private bool _intro = false;
    private bool _canSave = false;

    // Use this for initialization
    void Start() {
        
        if (!DataBase.db.getIntroZ()) {
            IntroZone();
            DataBase.db.setIntroZ(true);
        }

        //aixo es una fucking semada
        else if (DataBase.db.getEnd(0)) {
            if (DataBase.db.getZone() == "Biblioteca") {
                if (!DataBase.db.getEnd(1) && !DataBase.db.getEnd(3)) {
                    Dialogos.TM.activarsequencia("End_1");
                    DataBase.db.setEnd(1, true);
                }
            }
            else if (DataBase.db.getZone() == "Arxiu") {
                if (DataBase.db.getEnd(1) && !DataBase.db.getEnd(3)) {
                    Dialogos.TM.activarsequencia("End_1");
                    DataBase.db.setEnd(2, true);
                    _end = true;
                }

            }
            else if (DataBase.db.getZone() == "Observatori") {
                if (!DataBase.db.getEnd(1) && !DataBase.db.getEnd(3)) {
                    Dialogos.TM.activarsequencia("End_2");
                    DataBase.db.setEnd(3, true);
                    _end = true;
                }
            }
        }
        if (_historiador) {
            DataBase.db.setHistoriador();
            CharacterManager.cM.refreshCharacters();
        }
    }

    // Update is called once per frame
    void Update() {
        if (_intro&&DataBase.db.getZone()!="Menu") {
            if (!DataBase.db.getIntro()) {
                Dialogos.TM.activarsequencia("Start");
                DataBase.db.setIntro(true);
                DataBase.db.setSequence(0, true);
                _canSave = true;
            }
        }

        if (_canSave) {
            if (DataBase.db.getZone() == "Hall" || DataBase.db.getZone() == "Passadis") {
                if (Dialogos.TM.endsequencia) {
                    if (DataBase.db.getZone() == "Hall" && DataBase.db.getSaves(0) == false) {
                        saveFeedback();
                        DataBase.db.setSaves(0);
                        DataBase.db.Save();
                    }
                    else if (DataBase.db.getSaves(1) == false) {
                        saveFeedback();
                        DataBase.db.setSaves(1);
                        DataBase.db.Save();
                    }
                }
            }
        }

        //finals
        if (_end) {
            if (DataBase.db.getZone() == "Arxiu") {
                if (DataBase.db.getEnd(2) && !DataBase.db.getEnd(3)) {
                    if (Dialogos.TM.endsequencia) {
                        StartCoroutine(EndShare());
                    }
                }
            }
            else if (DataBase.db.getZone() == "Observatori") {
                if (!DataBase.db.getEnd(1) && DataBase.db.getEnd(3)) {
                    if (Dialogos.TM.endsequencia) {
                        StartCoroutine(EndShare());
                    }
                }
            }
        }
    }


    private bool IntroZone() {
		GameMaster.GM.SetCanWalk (false);
        ActiveDialogos.AD.IntroZone(true);
        Text textCanvas;
        GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
        textCanvas = canvasObject.transform.Find("Introducio/Text").GetComponent<Text>();
        string textZone = "";

        if (DataBase.db.getLanguageStr()=="catala"){
            string _zone = DataBase.db.getZone();
            if (_zone == "Biblioteca") {
                textZone = "Biblioteca de Lletres";
            }
            else if (_zone == "Arxiu") {
                textZone = "Sala de manuscrits";
            }
            else if (_zone == "Passadis") {
                textZone = "Passadís";
            }
            else if (_zone == "Observatori") {
                textZone = "Torre de ciències";
            }
            else if (_zone == "Paraninf") {
                textZone = "Paranimf";
            }
            else if (_zone == "Hall") {
                textZone = _zone;
            }
        }

        else if (DataBase.db.getLanguageStr() == "castella") {
            string _zone = DataBase.db.getZone();
            if (_zone == "Biblioteca") {
                textZone = "Biblioteca de Letras";
            }
            else if (_zone == "Arxiu") {
                textZone = "Sala de manuscritos";
            }
            else if (_zone == "Passadis") {
                textZone = "Pasillo";
            }
            else if (_zone == "Observatori") {
                textZone = "Torre de ciencias";
            }
            else if (_zone == "Paraninf") {
                textZone = "Paraninfo";
            }
            else if (_zone == "Hall") {
                textZone = _zone;
            }
        }

        else {
            string _zone = DataBase.db.getZone();
            if (_zone == "Biblioteca") {
                textZone = "Arts Library";
            }
            else if (_zone == "Arxiu") {
                textZone = "Manuscript room";
            }
            else if (_zone == "Passadis") {
                textZone = "Hallway";
            }
            else if (_zone == "Observatori") {
                textZone = "Science tower";
            }
            else if (_zone == "Paraninf") {
                textZone = "Assembly Hall";
            }
            else if (_zone == "Hall") {
                textZone = _zone;
            }
        }
        
        textCanvas.text = textZone;
        StartCoroutine(waitZone());
        return true;
    }

    private IEnumerator waitZone() {
        yield return new WaitForSeconds(1.5f);
        ActiveDialogos.AD.IntroZone(false);
        _intro = true;
        yield break;
    }

    private void saveFeedback() {
        ActiveDialogos.AD.Save(true);
        StartCoroutine(waitSave());
        //cmon fucking git don't play games with me
    }

    private IEnumerator waitSave() {
        Text textCanvas;
        GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
        textCanvas = canvasObject.transform.Find("Save/Text").GetComponent<Text>();
        int lng = DataBase.db.getLanguage();

        if (lng == 0) textCanvas.text = "Guardant...";
        if (lng == 1) textCanvas.text = "Guardando...";
        if (lng == 2) textCanvas.text = "Saving...";
        yield return new WaitForSeconds(1.0f);
        if (lng == 0) textCanvas.text = "Guardat";
        if (lng == 1) textCanvas.text = "Guardado";
        if (lng == 2) textCanvas.text = "Saved";
        yield return new WaitForSeconds(1.0f);
        ActiveDialogos.AD.Save(false);
        yield break;
    }

    private IEnumerator EndShare() {
        FaceBookManager.FbM.Ready2Share(2);
        yield return new WaitForSeconds(5.0f);
		DataBase.db.setZone ("Menu");
		GameMaster.GM.DestroyGameMaster ();
		switch (DataBase.db.getLanguage()) {
		case 0:
			SceneManager.LoadScene("Credits", LoadSceneMode.Single);
			break;
		case 1:
			SceneManager.LoadScene("ES_Credits", LoadSceneMode.Single);
			break;
		case 2:
			SceneManager.LoadScene("EN_Credits", LoadSceneMode.Single);
			break;
		}
        yield break;
    }
}
