﻿using UnityEngine;
using System.Collections;

public class ActiveDialogos : MonoBehaviour {
	public GameObject quadreDialeg;
    public GameObject Quadre1;
    public GameObject Portrait1;
    public GameObject QuadreM;
    public GameObject PortraitM;
    public GameObject Introduction;
    public GameObject SaveQuadre;
    public static ActiveDialogos AD;
    // Use this for initialization
    void Awake() {
        AD = this;
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Active(bool boolea){
		quadreDialeg.SetActive(boolea);
	}

    public void ActiveMirror(bool _state) {
        if (_state) {
            Quadre1.SetActive(false);
            Portrait1.SetActive(false);
            QuadreM.SetActive(true);
            PortraitM.SetActive(true);
        }
        else {
            Quadre1.SetActive(true);
            Portrait1.SetActive(true);
            QuadreM.SetActive(false);
            PortraitM.SetActive(false);
        }
    }

    public void LeChuck() {
        Portrait1.SetActive(false);
        PortraitM.SetActive(false);
    }

    public void IntroZone(bool state) {
        Introduction.SetActive(state);
    }

    public void Save(bool state) {
        SaveQuadre.SetActive(state);
    }
}
