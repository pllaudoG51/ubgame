﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Item : MonoBehaviour {
	public string _Name;
	public bool _Combine;
	public Transform _click;
	public float distance= 1.5f;
	public string objective = "";
	public int idle;
	public bool especial;
	//Item temp;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
	}	

	void OnMouseOver(){
		//show on
		if(!GameMaster.GM.getDialeg()){
			if(!PuzzleManager.PM.GetActivePuzzle()){
				if(Input.GetMouseButtonDown(0)){//mouse reliese over the item
					if (Vector2.Distance(transform.position, GameObject.Find("Player").transform.position) < distance) {
						GameObject.Find("Player").GetComponent<Player>().CatchObject();
						StartCoroutine(WaitForItem());
						OnMouseExit();
						//say to database to not spawn it again

					}
					else GameMaster.GM.Walk(_click, idle);
					
				}
			}
		}
	}
		
	private IEnumerator WaitForItem(){
		ItemManager.IM.setObject(_Name);
		yield return new WaitForSeconds(0.85f);
		if(!especial)DataBase.db.AddItem (this);
		else DataBase.db.checkEspecialAdd(1);//nomes 1 per que nomes hi ha un cas jojo
		Destroy(gameObject);
		yield break;
	}
		

	void OnMouseEnter(){
		CursorManager.CM.OnCursorEnter("interactive");
	}

	void OnMouseExit(){
		CursorManager.CM.OnCursorExit();
	}

}
