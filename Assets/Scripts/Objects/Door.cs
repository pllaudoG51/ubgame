﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour {
	private bool Open = false;//de oment publica fins tenir la DB
	private bool Player = false;//vol dir que estem en contacte amb el player
	private bool clicked =false;
	private bool Important;//si la porta es important i es deles que s'han d'interactuar
	private bool _once=false;//quan no es visible que només s'activi el dialeg un cop
	private bool _flag = true;
    private bool countdown = false;

	public int idle;
	public bool Closed = false; //per si no es important
	public string _nom;
	public bool visible = true;
	public string posicioIDLE;
	public string connection;//nom de la seguent escena
	public string connectionDoor;
	public Transform ClickTransform;

	// Use this for initialization
	void Start () {
		///comprovem l'estat de la porta
		//DataBase.db.CheckDoor();
		Important = DataBase.db.CheckImportant(connectionDoor);
		if(Important)Open = DataBase.db.CheckDoor(connectionDoor);
		if(visible)CheckSprite();
	}
	
	// Update is called once per frame
	void Update () {
        if (countdown) {
            if (Dialogos.TM.endsequencia) {
				DataBase.db.setZone ("Menu");
				GameMaster.GM.DestroyGameMaster ();
				switch (DataBase.db.getLanguage()) {
				case 0:
					SceneManager.LoadScene("Credits", LoadSceneMode.Single);
					break;
				case 1:
					SceneManager.LoadScene("ES_Credits", LoadSceneMode.Single);
					break;
				case 2:
					SceneManager.LoadScene("EN_Credits", LoadSceneMode.Single);
					break;
				}
            }
        }
    }


    /// <summary>
    /// Si la porta esta tancada i utilitzes l'objecte s'obre,
    ///  mira com enllaçar objecte i porta
    /// </summary>
    public void OpenDoor(){
		DataBase.db.OpenDoor(connectionDoor);
		Open = true;
		if(visible)CheckSprite();
		GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("OpenDoor");

		//canvi de database;
	}


	/// <summary>
	/// Si la porta esta oberta aniras a on vagis
	/// </summary>
	public void GoTo(){
		if(_flag){
			_flag = false;
			if(connection=="End")EndOfGame();
			else {
				GameObject.Find ("SoundMaster").GetComponent<SoundMaster> ().PlayClip ("UseDoor");
				Fading.FD.canvidemapacrida();
				StartCoroutine(ChangeEscene());
			}
		}
	}


	private IEnumerator ChangeEscene(){
		yield return new WaitForSeconds(1.5f);
		GameMaster.GM.SetActualDoor(connectionDoor);
		DataBase.db.setZone(connection);
		SceneManager.LoadScene(connection, LoadSceneMode.Single);
		yield break;
	}

	private void EndOfGame(){
        Dialogos.TM.activarsequencia("End1");
        countdown = true;
		//funcio que cridara tot el que hagi de cridar per acabar el joc!
	}

	/// <summary>
	/// Funcio que examines la porta i surt dialeg quan no esta oberta, 
	/// enllaç a dialeg json que haure de parlar amb el pere
	/// </summary>
	public void ShowDoor(){
        Dialogos.TM.activarsequencia(_nom);
	}

	/// <summary>
	/// Funcio general que gestiona tot el fet de la porta i les multiples opcions
	/// </summary>
	public void CheckDoor(){
		if(Important){
			if(Open)GoTo();
			else ShowDoor();
		}
		else {
			if(!Closed)GoTo();
			else  Dialogos.TM.activarCartell("Nope");///que la porta esta tancada
		}
		clicked=false;//per saber si s'hi ha anat amb un click ala porta o no (per portes que no es veuen)
	}

	private void CheckSprite(){
		if(Open) GetComponent<SpriteRenderer>().sprite = Resources.Load("Items/Door/"+connection+"_open", typeof(Sprite)) as Sprite;
		else GetComponent<SpriteRenderer>().sprite = Resources.Load("Items/Door/"+connection+"_close", typeof(Sprite)) as Sprite;
	}
		

	void OnMouseDown(){
		if(!GameMaster.GM.getDialeg()){
			if(!PuzzleManager.PM.GetActivePuzzle()){
				if (!ItemManager.IM.getAnyItemSelected()) {
					if(Player)CheckDoor();
					else {
						GameMaster.GM.Walk(ClickTransform, idle);
						clicked = true;
					}
				}
			}
		}
	}

	public void resetClicked(){
		clicked = false;
	}

	/// <summary>
	/// TOTS els triggers
	/// </summary>
	/// <param name="other">Other.</param>

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player"){
			Player = true;
			if(!visible && clicked && !_once){
				CheckDoor();
				_once=true;
			}

		}
	}

	void OnTriggerExit2D(Collider2D other){
		if(other.tag == "Player"){
			Player = false;
		}
	}

	//ANOTACIO COMPANY 
	//el funcionament del script es senzill, tenim 3 funcions openDoor es la funcio que s'haura de cridar per obrir la porta
	//ShowDoor es la funcio que activar el dialeg de quan una porta estigui tancada
	//i GOTO es la funcio per quan estigui oberta anar a l'escena que te conectada amb el connection
	//Llavors tenim la funcio checkdoor que quan cliquem a la porta activa la funció checkdoor i enllaça les altres funcions
	//Per poder activar-ho em d'estar en contacte amb el jugador
	//ANOTACIO PER A POL: Per obrir la porta, has de trobar la porta especfica i cridar la funcio OpenDoor
	//Tot el tema de les animacions ja veurem que fem potser la porta hauria de ser un Asset apart que estigui oberta i tancada?
	void OnMouseEnter(){
		CursorManager.CM.OnCursorEnter("door");
	}

	void OnMouseExit(){
		CursorManager.CM.OnCursorExit();
	}

	public string getConnection(){
		return connectionDoor;
	}

	public string getPositionIDLE(){
		return posicioIDLE;
	}
}
