﻿using UnityEngine;
using System.Collections;

public class Element : MonoBehaviour {
	public float distance= 1.5f;
	public bool usable; // if usable the element is  to use an item on it
						//if not usable the element is just to interact with the player
	public Item _simbiosis;//if usable it's the item that you need to activate it
	public Transform _click;						//if not usable is the item that it will drop
	public int idle;
	public bool interactuable = false;
	public string _nom; //nom objecte que crida funcio
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseOver()
    {
        if (!GameMaster.GM.getDialeg())
        {
            if (!PuzzleManager.PM.GetActivePuzzle())
            {

                if (Input.GetMouseButtonDown(0))
                {
                    if (Vector2.Distance(transform.position, GameObject.Find("Player").transform.position) < distance)
                    {
                        if (usable)
                        {
                            if (ItemManager.IM.getAnyItemSelected())
                            {
                                //make it glow
                                if (_simbiosis._Name == GameObject.Find("Master").GetComponent<ItemManager>().aux._Name)
                                {
                                    //use it
									int i = DataBase.db.ItemPos(ItemManager.IM.aux);
                                    GameObject.Find("SoundMaster").GetComponent<SoundMaster>().PlayClip("UseItem");
									DataBase.db.RemoveItem(i);
                                }
                                else
                                {
                                    //I cant use that here
                                    Dialogos.TM.examineItem("Nope");
                                }
                                ItemManager.IM.setAnyItemSelected(false);
                            }

                        }
                        else
                        {//not usable
                            if (!GameMaster.GM.Walking())
                            {
                                //ho faig perque es puguin tenir elements interactuables, sino s'ha de tenir en compte que sigui un objecte important
                                if (interactuable) Invoke(_nom, 0);
                                else if (ItemManager.IM.CheckObject(this.name)) Invoke(_nom, 0);
                            }
                        }
                    }
                    else
                    {
                        GameMaster.GM.Walk(_click, idle);
                    }
                }
            }
        }
    }


    void OnMouseEnter(){
		if(interactuable)CursorManager.CM.OnCursorEnter("interactive");
		else if(ItemManager.IM.CheckObject(this.name))CursorManager.CM.OnCursorEnter("interactive");
	}

	void OnMouseExit(){
		CursorManager.CM.OnCursorExit();
	}


	//llistat de funcions per cada element (hardcoded)
	void ArxiuClau(){
		GameObject.Find("Player").GetComponent<Player>().CatchObject();
		StartCoroutine(CooldownCatch());
		Dialogos.TM.activarCartell ("CatchArxiu");
		OnMouseExit();
	}

	void Pala(){
		Dialogos.TM.activarsequencia("Pala");
		GameObject.Find("Player").GetComponent<Player>().CatchObject();
		StartCoroutine(CooldownCatch());
		GameObject.Find ("Escenari/Background").GetComponent<ChangeSpritePassadis> ().ChangePala (false);
		OnMouseExit();
	}
		

	private IEnumerator CooldownCatch(){
		ItemManager.IM.setObject(_nom);//per a que només es pugui agafar un cop
		yield return new WaitForSeconds(0.85f);
		DataBase.db.AddItem (_simbiosis);
		yield break;
	}

	void PortaArxiu(){
		//si en tot cas fiquem avśi porta arxiu
		Dialogos.TM.activarCartell ("PortaArxiu");
	}

	void Llibreta(){
		Dialogos.TM.examineItem(this.name);
	}

	void Quadre(){
		Dialogos.TM.examineItem (this.name);
	}
    void LeChuck() {
        Dialogos.TM.activarsequencia(this.name);
    }

	void Radev(){
		Dialogos.TM.examineItem(this.name);
	}
    //end of llistat



}
