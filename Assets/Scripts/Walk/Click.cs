﻿using UnityEngine;
using System.Collections;

public class Click : MonoBehaviour {
	private int Part = 1;
	private bool CentralPart = false;
	private int _id;
	// Use this for initialization
	void Start () {
		//GameObject.Find ("Player").GetComponent<MovePlayer>().rotacio=true;
		//if(GameObject.Find ("GameMaster").GetComponent<GameMaster>().utilitzaciocolumn){
		//	GameObject.Find ("GameMaster").GetComponent<GameMaster>().columnfalse();
		//}
		//
		//GameObject.Find ("Player").GetComponent<MovePlayer>().objectiufalse();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
	
	void OnTriggerEnter2D(Collider2D other){



		if(other.gameObject.tag=="CentralPart"){
			_id = other.GetComponent<MouseDetect>().getID();
			CentralPart = true;
			AssignDestination();
			//enviem a Escene Master que desti es aquest numero
			//per tant un cop aixo haurem de passar desde el numero mes proxim i anar fins el desti.
			
		}
		if (other.gameObject.tag == "Obstacles") {
			_id = other.GetComponent<MouseDetect>().getID();
			AssignDestination();
		}
			
	}
	
	public void destroy(){
		Destroy (gameObject);
	}

	public int getPartNum(){
		return Part;
	}

	public bool getCentralPart(){
		//retorna si estem a la zona que vas directe o no
		return CentralPart;//de moment false fins que ho faci
	}



	private void AssignDestination(){
		float distance = 1000000.0f;//definim una primera distancia amb molt valor
		///Busquem tots les Parts per determinar quin esta mes aprop del click
		GameObject[] go = GameObject.FindGameObjectsWithTag ("Part");
		foreach (GameObject part in go){
			if (Vector2.Distance(transform.position, part.transform.position)<distance ){
				distance = Vector2.Distance(transform.position, part.transform.position);
				Part = part.GetComponent<Part>().getPart();
			}
		}
		GameObject.Find ("Player").GetComponent<Player>().CanMove();
	}





	public int getID(){
		return _id;
	}

}

