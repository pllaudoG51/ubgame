﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	private int NextStep;
	private int PartNum;
	private bool CentralPart = false;
	private Transform GoTo;//direccio cap a on em d'anar
	private bool move = false;
    private Click cl;
	private Animator _anim;
	private int _animation=0;//0 down 1 top 2 horizontal
	private bool _left = false;
	public bool _observer =false;
	public int _id = 0;

	public float Speed = 0.5f;
	// Use this for initialization
	void Start () {
		NextStep = 0;
		_anim = this.GetComponent<Animator>();
		CheckActualPart();//no se si fara falta actualitzar-ho cada vegada

	}
	
	// Update is called once per frame
	void Update () {

        if (move){
		//	ChangeDestination();
            if (GoTo != null)  {
                transform.position = Vector3.MoveTowards(transform.position, GoTo.position, Time.deltaTime * Speed);
                if (Vector2.Distance(transform.position, GoTo.position) < 0.01f) {
                    if (Vector2.Distance(transform.position, cl.transform.position) < 0.1f) {
						resetAnimations();
						checkIDLE();
						if(GameMaster.GM.getChangeIDLE())DefineIDLE(GameMaster.GM.CheckIDLE());
                        cl.destroy();
                        move = false;
                    }
                    else ChangeDestination();
                }
            }

        }
	}


	/// <summary>
	/// Funcio que canvia la direccio quan hi ha un nou click
	/// </summary>
	private void ChangeDestination(){
		CheckActualPart();//actualitzem el PArtNum, que es el part mes proxima que te el jugador

		/*
		Opmitzat el sistema,
		es comprova primer en quines zones s'esta i després fem calculs
		*/
		//si estan en la zona central va tot directe
		if(cl.getID() == _id){
			GoTo = GameObject.Find ("Click(Clone)").transform;
		}
		else{
			if(CentralPart && cl.getCentralPart()){

				GoTo = GameObject.Find ("Click(Clone)").transform;
			}
			else if(CentralPart && !cl.getCentralPart()){

				//si estan en posicions diferents
				if(PartNum==cl.getPartNum()){
					//si estan en la mateixa alçada o estan en el mateix part
					//si la distancia es petita que hi vagi directe
					//sino que vagi a la part del click
					//ho he fet aixi per evitar aquelles vegades que anava directe i es saltava el collider
					//mira si 0.5 es poc o molt
					if (Vector2.Distance(transform.position, getNextPosition(cl.getPartNum()).position)<0.5f)GoTo = GameObject.Find ("Click(Clone)").transform;
					else {

						GoTo = getNextPosition(cl.getPartNum());
					}
			
				}
				//vol dir que el personatge esta a la part central i el click en obstacles
				else{	
					
					NextStep = cl.getPartNum();
					GoTo = getNextPosition(NextStep);
				}
			}
			else if(!CentralPart && cl.getCentralPart()){
				//si el personatge esta a la zona d'obstacles i el click esta a la zona central doncs anem cap a la part del jugador
				GoTo = getNextPosition(PartNum);
			
			}
			else {
				
				if(PartNum==cl.getPartNum())GoTo = GameObject.Find ("Click(Clone)").transform;
				else GoTo = getNextPosition(PartNum);
				//GoTo = GameObject.Find ("Click(Clone)").transform;
			}
		}
		CheckAnimation();		
	}
		
    /*
    Retorna la posició següent fent un find de la part
    */

    public Transform getNextPosition(int pos){
        return GameObject.Find("Part" + pos).transform;
    }

    private void CheckActualPart(){
		///mirem quina part es la que esta mes aprop
		float distance = 1000000.0f;//definim una primera distancia amb molt valor
		GameObject[] go = GameObject.FindGameObjectsWithTag ("Part");
		foreach (GameObject part in go){
			if (Vector2.Distance(transform.position, part.transform.position)<distance ){
				distance = Vector2.Distance(transform.position, part.transform.position);
				PartNum = part.GetComponent<Part>().getPart();
			}
		}
	}

	/// <summary>
	/// Funcio que determina quina animació s'ha de fer servir quan s'invoca un click
	/// </summary>
	private void CheckAnimation(){
		resetAnimations();
		initMovement();
		float distanceX = Mathf.Abs(transform.position.x- GoTo.position.x);
		float distanceY = Mathf.Abs(transform.position.y- GoTo.position.y);
		if(distanceY>distanceX){
			if(transform.position.y < GoTo.position.y){
				_anim.SetBool("GoTop", true);
				_animation =1;
			}
			else{
				_anim.SetBool("GoDown", true);
				_animation = 0;
			}
		}
		else{
			if(transform.position.x < GoTo.position.x){
				transform.rotation=Quaternion.Euler (transform.rotation.x,180,transform.rotation.z);
				_anim.SetBool("Horizontal", true);
				_left = false;
			}
			else{
				transform.rotation=Quaternion.Euler (transform.rotation.x,0,transform.rotation.z);
				_anim.SetBool("Horizontal", true);
				_left = true;
			}
			_animation=2;
		}
	}

	private void resetAnimations(){
		_anim.SetBool("GoDown", false);
		_anim.SetBool("GoTop", false);
		_anim.SetBool("Horizontal", false);
	}

	private void initMovement(){
		_anim.SetBool("IDLETop", false);
		_anim.SetBool("IDLEDown", false);
		_anim.SetBool("IDLEHorizontal", false);
	}

	private void checkIDLE(){
		if(_animation==0)_anim.SetBool("IDLEDown", true);
		else if(_animation==1)_anim.SetBool("IDLETop", true);
		else if(_animation==2)	{
			_anim.SetBool("IDLEHorizontal", true);
			if(_left)transform.rotation=Quaternion.Euler (transform.rotation.x, 180,transform.rotation.z);
			else transform.rotation=Quaternion.Euler (transform.rotation.x, 0,transform.rotation.z);
		}
	}

    /*
    Funcions per limitar el moviment del jugador
     */

	public void CanMove(){
        cl = GameObject.Find("Click(Clone)").GetComponent<Click>();
		CheckActualPart();
		ChangeDestination();//li diem cap a on ha d'anar
		move = true;

	}

	public void CantMove(){
		move = false;
	}




	/// <summary>
	/// Getters
	/// </summary>

	public int getNumPart(){
		//retorna a quina part de l'escenari esta mes proxima
		return PartNum;
	}

	public bool getCentralPart(){
		return CentralPart;
	}

	public int getAnimation(){
		if(_animation == 2){
			if(_left)return 3;
		}
		return _animation;
	}

	/////ANOTACIO funcionament moviment jugador
	/// Definir dos parts, la parts amb obstacles que tindran punts pels quals es mouran i 
	/// la part central o sense obstacles que es moure directe al click
	/// PASSOS per a fer la comprovacio d'on em d'anar
	/// Primer de tot mirem si em d'anar directament a click o no (per tant no cal la resta de passos)
	/// Segon movem el player cap el partnum del player
	/// Tercer cridem altre cop el checkgotoclick
	/// Quart si dona false cridem el calculatenextstep que ens dira cap a on em d'anar
	/// Cinque moure el jugador cap el pase seguent
	/// Sise tornem al pas 3


	///////Triggers
	/// 
	/// 
	private void OnTriggerStay2D(Collider2D other){
		if (other.gameObject.tag == "CentralPart") {
			_id = other.GetComponent<MouseDetect>().getID();
			CentralPart = true;
		}
		if (other.gameObject.tag == "Obstacles") {
			_id = other.GetComponent<MouseDetect>().getID();
		}
	}
	private void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "CentralPart") {
			_id = other.GetComponent<MouseDetect>().getID();
			CentralPart = true;
		}
		if(other.tag == "Borde"){
			CameraPosition.CM.setStop(true);
		}
	}

	private void OnTriggerExit2D(Collider2D other){
		
		if (other.gameObject.tag == "CentralPart") {
			
			CentralPart = false;
		}
		if (other.gameObject.tag == "Observacio") {
			_observer = false;
		}
		if(other.tag == "Borde"){
			CameraPosition.CM.setStop(false);
		}
	}

	///////////////////funcions animacions///////////////
	/// canvia el IDLE segons convingui al canvi d'escena
	public void DefineIDLE(int animation){
		_animation = animation;
		if(animation == 3){
			_left = true;
			_animation =2;
		}
		else _left = false;
		_anim = this.GetComponent<Animator>();
		_anim.SetBool("IDLEDown", false);
		initMovement();
		checkIDLE();
	}
	/// <summary>
	/// funcio que comprova quina animació s'ha de fer per agafar objecte
	/// </summary>
	public void CatchObject(){
		//Debug.Log("entraaaa");
		switch (_animation){
		case 0:
			_anim.SetBool("ObjectDown", true);
			break;
		case 1:
			_anim.SetBool("ObjectTop", true);
			break;
		case 2:
			_anim.SetBool("ObjectHorizontal", true);
			break;
		}
	}

	public void resetObject(){
		_anim.SetBool("ObjectDown", false);
		_anim.SetBool("ObjectTop", false);
		_anim.SetBool("ObjectHorizontal", false);
	}
}
