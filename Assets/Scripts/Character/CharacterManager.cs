﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class CharacterManager : MonoBehaviour {
	public static CharacterManager cM;

	private int[] Characters = new int[4];
	//Numero 1 -> Informatic
	//Numero 2 -> Biologoa
	//Numero 3-> Lletres
	//Numero 4 ->Art/Historiador
	private int ActiveCharacter = 1;
	// Use this for initialization
	void Awake() {
		if (cM == null) {
			DontDestroyOnLoad(gameObject);
			cM = this;
		}
		else if (cM != this) {
			Destroy(gameObject);
		}
	}

	void Start(){
		initCharacters();
		DrawCharacters();
	}
	
	// Update is called once per frame
	void Update () {
	}


	/// <summary>
	/// Ordenem el personatge actiu
	/// </summary>
	private void SortCharacter(){
		int i = 1;
		int[] temp = new int[4];
		for (int x = 1; x < 5; x++){
			if(x==ActiveCharacter)temp[0] = x;
			else{
				temp[i]=x;
				i++;
			}
		}
		Characters=temp;
	}

	/// <summary>
	/// Canviem el personatge actiu que podem utilitzar
	/// </summary>
	public void ChangeCharacter(int active){
		if(DataBase.db.getHistoriador()){
			ActiveCharacter = Characters[active-1];
			SortCharacter();
			DrawCharacters();
		}
		else {
			if(active<4){
				ActiveCharacter = Characters[active-1];
				SortCharacter();
				DrawCharacters();
			}
		}
	}

	/// <summary>
	///	Funcio que al clicar en el personatge actiu l'utilitza
	/// </summary>
	public void UseCharacter(){	
		if(!GameMaster.GM.getDialeg()){
			if(ActiveCharacter == PuzzleManager.PM.getPersonatge())Dialogos.TM.activarPistaPuzzle(ClueManager.cM.getClueNum().ToString());
			else Dialogos.TM.activarPistaPuzzle("no", "Character"+ (ActiveCharacter+1).ToString());
			//Debug.Log("Pista" + ClueManager.cM.getClueNum() + "Personatge" + getCharacterName());

		}
	}

	/// <summary>
	/// Inicialitzem el array de personatges
	/// </summary>
	private void initCharacters(){
		for (int  i = 0; i < 4; i++)Characters[i]=i+1;
	}

	public void refreshCharacters(){
		SortCharacter();
		DrawCharacters();
	}

	/// <summary>
	/// Assignem la nova imatge a cada quadre
	/// </summary>
	private void DrawCharacters(){
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		int characters = 0;
		if(DataBase.db.getHistoriador()) characters = 4;
		else characters = 3;
		for(int i =0; i<characters; i++){
			Image imageCanvas = canvasObject.transform.Find("Characters/Character"+(i+1)).GetComponent<Image>();
			Sprite imatge;
			if(i==0)imatge = Resources.Load("Characters/Character_Gran"+Characters[i], typeof(Sprite)) as Sprite;
			else imatge = Resources.Load("Characters/Character_Petit"+Characters[i], typeof(Sprite)) as Sprite;
			imageCanvas.sprite = imatge;
		}
	}

	/// <summary>
	/// Retorna el nom envers el Id
	/// </summary>

	public string getCharacterName(){
		if(ActiveCharacter == 1)return "biologa";
		else if(ActiveCharacter == 2)return "lletres";
		else if(ActiveCharacter ==3)return "artista";
		else if (ActiveCharacter ==4)return "historiador";
		return "pene";
	}


	public void paint(){
		for (int i = 0; i < 4; i++){
			//Debug.Log(Characters[i]);
		}
	}
}
