﻿using UnityEngine;
using System.Collections;

public class CharacterChange : MonoBehaviour {

	// Use this for initialization
	void Start () {
		CharacterManager.cM.refreshCharacters();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Change(int character){
		if(!GameMaster.GM.getDialeg() && !GameMaster.GM.getPause()){
			CharacterManager.cM.ChangeCharacter(character);
			SoundMaster.instance.PlayClip("Click");
		}

	}

	public void Use(){
		if(!GameMaster.GM.getDialeg() && !GameMaster.GM.getPause()){
			CharacterManager.cM.UseCharacter();
			SoundMaster.instance.PlayClip("Character");
		}
	}
}
